package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "user")

public class User {
    @PrimaryKey
    public int id;

    public String email;

    public String firstname;

    public String lastname;

    public Double gpa;
}
package com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.collegeplanner.collegeplanner.ClassActivity;
import com.collegeplanner.collegeplanner.Helper.AdapterRecyclerClassTask;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentClassTask extends Fragment implements ClassActivity.OnAboutDataReceivedListener{

    private RecyclerView mRecyclerView;
    private AdapterRecyclerClassTask mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String class_id;
    private String class_name;

    public FragmentClassTask(){
        super();
    }

    @SuppressLint("ValidFragment")
    public FragmentClassTask(String class_id, String class_name) {
        super();
        this.class_id = class_id;
        this.class_name = class_name;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_class_task, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        FloatingActionButton fab = view.findViewById(R.id.fab_addClassTask);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO ADD CLASS TASK
            }
        });

        mRecyclerView = view.findViewById(R.id.cta_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AdapterRecyclerClassTask(getContext(), new AdapterRecyclerClassTask.RecyclerViewClickListener(){
            @Override
            public void onClick(View view, int position) {
                //TODO EDIT/DELETE CLASSS TASK
            }
        });

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDataReceived(Object data) {
        try {
            JSONArray jsonArray = (JSONArray) data;
            ArrayList<Map<String, String>> dataTimes = new ArrayList<>();
            for (int i=0; i<jsonArray.length(); i++) {
                JSONObject array = jsonArray.getJSONObject(i);
                String id = array.getString("id");
                String name = array.getString("name");
                String deadline_date = array.getString("deadline_date");
                String deadline_time = array.getString("deadline_time");
                String detail = array.getString("detail");
                String img = array.getString("img");
                String class_id = array.getString("class_id");
                String class_name = array.getString("class_name");
                String semester_id = array.getString("semester_id");
                String year_id = array.getString("year_id");
                String class_color = array.getString("class_color");

                Map<String, String> dataTime = new HashMap<>();
                dataTime.put("id", id);
                dataTime.put("name", name);
                dataTime.put("deadline_date", deadline_date);
                dataTime.put("deadline_time", deadline_time);
                dataTime.put("detail", detail);
                dataTime.put("img", img);
                dataTime.put("class_id", class_id);
                dataTime.put("class_name", class_name);
                dataTime.put("semester_id", semester_id);
                dataTime.put("year_id", year_id);
                dataTime.put("class_color", class_color);
                dataTimes.add(dataTime);
            }
            mAdapter.updateData(dataTimes);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

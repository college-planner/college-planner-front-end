package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "class", foreignKeys = @ForeignKey(entity = Semester.class,
    parentColumns = "id",
    childColumns = "semester_id")
)

public class Class {
    @PrimaryKey
    public int id;

    public int semester_id;

    public String name;

    public String detail;

    public String color;

    public Double gpa;

    public int credit;


}
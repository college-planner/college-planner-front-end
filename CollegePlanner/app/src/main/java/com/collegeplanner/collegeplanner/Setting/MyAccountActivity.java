package com.collegeplanner.collegeplanner.Setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyAccountActivity extends AppCompatActivity
{

    TextView textName;
    Button buttonChangeUsername;
    Button buttonChangePassword;
    Button buttonChangeEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_my_account);

        textName = findViewById(R.id.sm_textName);
        buttonChangeUsername = findViewById(R.id.sm_buttonChangeUsername);
        buttonChangePassword = findViewById(R.id.sm_buttonChangePassword);
        buttonChangeEmail = findViewById(R.id.sm_buttonChangeEmail);

        buttonChangeUsername.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(MyAccountActivity.this, ChangeNameActivity.class);
                startActivityForResult(i, 1);
                getHeader();
            }
        });

        buttonChangePassword.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyAccountActivity.this, ChangePasswordActivity.class);
                startActivity(i);
            }
        });


        buttonChangeEmail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(MyAccountActivity.this, ChangeEmailActivity.class);
                startActivity(i);
            }
        });

        getHeader();
    }

    private void getHeader() {
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    //String message = o.getString("message");
                    if(status.equalsIgnoreCase("true")){
                        JSONObject jsonObject = new JSONObject(o.getString("data"));
                        textName.setText(jsonObject.getString("firstname") + " " + jsonObject.getString("lastname"));
                    }
                    else if(status.equalsIgnoreCase("false")){ //jika data 0 masuk sini juga
                        //String message = o.getString("message");
                        //Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(MyAccountActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(MyAccountActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(MyAccountActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "user/user";

        String ApiKey = this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(this).get(url, ofl, params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1){
            getHeader();
        }
    }
}

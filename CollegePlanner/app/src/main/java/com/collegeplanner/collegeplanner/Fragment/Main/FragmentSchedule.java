package com.collegeplanner.collegeplanner.Fragment.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.collegeplanner.collegeplanner.AddClassActivity;
import com.collegeplanner.collegeplanner.R;

public class FragmentSchedule extends AbstractFragmentMain {

    Button addClass;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_schedule, parent, false);

        addClass = (Button) v.findViewById(R.id.btnAdd);

        return v;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        addClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddClassActivity.class);
                startActivity(i);
            }
        });
    }
}

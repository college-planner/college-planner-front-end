package com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.collegeplanner.collegeplanner.AddTimeActivity;
import com.collegeplanner.collegeplanner.ClassActivity;
import com.collegeplanner.collegeplanner.Helper.AdapterRecyclerClassTime;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentClassTime extends Fragment implements ClassActivity.OnAboutDataReceivedListener{

    private RecyclerView mRecyclerView;
    private AdapterRecyclerClassTime mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String class_id;
    private String class_name;
    private ArrayList<Map<String, String>> dataTimes = new ArrayList<>();

    public FragmentClassTime(){
        super();
    }

    @SuppressLint("ValidFragment")
    public FragmentClassTime(String class_id, String class_name) {
        super();
        this.class_id = class_id;
        this.class_name = class_name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_class_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        FloatingActionButton fab = view.findViewById(R.id.fab_addClassTime);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FragmentClassTime.this.getActivity(), AddTimeActivity.class);
                i.putExtra("class_id", class_id);
                i.putExtra("class_name", class_name);
                startActivityForResult(i, 1);
            }
        });

        mRecyclerView = view.findViewById(R.id.cti_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AdapterRecyclerClassTime(getContext(), new AdapterRecyclerClassTime.RecyclerViewClickListener(){
            @Override
            public void onClick(View view, int position) {
                //TODO EDIT/DELETE CLASSS TIME
            }
        });

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            ((ClassActivity)getActivity()).showProgress(true);
            ((ClassActivity)getActivity()).getClassData();
        }
    }

    @Override
    public void onDataReceived(Object data) {
        dataTimes.clear();
        try {
            JSONArray jsonArray = (JSONArray) data;
            for (int i=0; i<jsonArray.length(); i++) {
                JSONObject array = jsonArray.getJSONObject(i);
                String id = array.getString("id");
                String days = array.getString("days");
                String location = array.getString("location");
                String start = array.getString("start");
                String end = array.getString("end");
                String class_id = array.getString("class_id");
                String class_name = array.getString("class_name");
                String semester_id = array.getString("semester_id");
                String year_id = array.getString("year_id");
                String class_color = array.getString("class_color");

                Map<String, String> dataTime = new HashMap<>();
                dataTime.put("id", id);
                dataTime.put("days", days);
                dataTime.put("location", location);
                dataTime.put("start", start);
                dataTime.put("end", end);
                dataTime.put("class_id", class_id);
                dataTime.put("class_name", class_name);
                dataTime.put("semester_id", semester_id);
                dataTime.put("year_id", year_id);
                dataTime.put("class_color", class_color);
                dataTimes.add(dataTime);
            }
            mAdapter.updateData(dataTimes);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

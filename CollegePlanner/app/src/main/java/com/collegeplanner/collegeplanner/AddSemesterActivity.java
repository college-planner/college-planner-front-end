package com.collegeplanner.collegeplanner;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddSemesterActivity extends AppCompatActivity {

    String year_id;
    TextView textStart, textEnd;
    Button buttonAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_semester);
        textStart = findViewById(R.id.as_start);
        textEnd = findViewById(R.id.as_end);
        buttonAdd = findViewById(R.id.as_add);

        Intent myIntent = getIntent();
        if (myIntent.getExtras() != null) {
            if (myIntent.hasExtra("idYear")) {
                year_id = myIntent.getStringExtra("idYear");
            }
        }
        textStart.setKeyListener(null);
        textStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDateTimePickerStart();
            }
        });
        textStart.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus)openDateTimePickerStart();
            }
        });

        textEnd.setKeyListener(null);
        textEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDateTimePickerEnd();
            }
        });
        textEnd.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus)openDateTimePickerEnd();
            }
        });


        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnFinishListener ofl = new OnFinishListener() {
                    @Override
                    public void onFinish(JSONObject o) {
                        try {
                            String status = o.getString("status");
                            String message = o.getString("message");
                            if (status.equalsIgnoreCase("true")) {
                                Toast.makeText(AddSemesterActivity.this, message, Toast.LENGTH_SHORT).show();

                                Intent i = new Intent(AddSemesterActivity.this, LoginActivity.class);
                                startActivity(i);
                                finish();
                            } else if (status.equalsIgnoreCase("false")) {
                                Toast.makeText(AddSemesterActivity.this, message, Toast.LENGTH_SHORT).show();
                            } else {
                                Log.e(getClass().getName(), "onFinish: " + o.toString());
                                Toast.makeText(AddSemesterActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                                Toast.makeText(AddSemesterActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(AddSemesterActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        }

                    }
                };
                String url = getString(R.string.webservice) + "Semester/insert_semester";

                final String ApiKey = AddSemesterActivity.this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");
                Map<String, String> params = new HashMap<>();
                params.put("key", ApiKey);
                params.put("year_id", year_id);
                params.put("start", textStart.getText().toString());
                params.put("end", textEnd.getText().toString());

                VolleyHelper.getInstance(AddSemesterActivity.this).post(url, ofl, params);
            }
        });
    }

    private void openDateTimePickerStart() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DATE);
        DatePickerDialog dtp = new DatePickerDialog(
            AddSemesterActivity.this,
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.date_format));
                    String formatted = simpleDateFormat.format(calendar.getTime());
                    textStart.setText(formatted);
                }
            },
            year,
            month,
            day
        );
        dtp.show();
    }

    private void openDateTimePickerEnd() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DATE);
        DatePickerDialog dtp = new DatePickerDialog(
            AddSemesterActivity.this,
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.date_format));
                    String formatted = simpleDateFormat.format(calendar.getTime());
                    textEnd.setText(formatted);
                }
            },
            year,
            month,
            day
        );
        dtp.show();
    }
}

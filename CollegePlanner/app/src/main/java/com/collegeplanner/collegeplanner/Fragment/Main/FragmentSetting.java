package com.collegeplanner.collegeplanner.Fragment.Main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.collegeplanner.collegeplanner.R;
import com.collegeplanner.collegeplanner.Setting.AboutActivity;
import com.collegeplanner.collegeplanner.Setting.MyAccountActivity;

public class FragmentSetting extends AbstractFragmentMain
{
    View person;
    View about;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_setting, parent, false);
        about = view.findViewById(R.id.about);
        person = view.findViewById(R.id.person);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyAccountActivity.class);
                startActivity(intent);
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AboutActivity.class);
                startActivity(i);
            }
        });
    }
}

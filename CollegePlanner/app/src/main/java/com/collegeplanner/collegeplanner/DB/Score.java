package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "score", foreignKeys = @ForeignKey(entity = Class.class,
        parentColumns = "id",
        childColumns = "class_id"))
@TypeConverters(DateConverter.class)
public class Score {
    @PrimaryKey
    public int id;

    public int class_id;

    public Date date;

    public String name;

    public String detail;

    public int score;

    public int weight;
}
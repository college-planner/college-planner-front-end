package com.collegeplanner.collegeplanner.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.collegeplanner.collegeplanner.LoginActivity;
import com.collegeplanner.collegeplanner.MainActivity;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*------------------------------------------------------------------------
-----------------------METHOD GET WIHTOUT PARAM---------------------------
--------------------------------------------------------------------------

OnFinishListener ofl = new OnFinishListener(){
    @Override
    public void onFinish( JSONObject o ) {
        //YOUR CODE HERE
    }
};
String url = getString(R.string.webservice) + "controller/method";

VolleyHelper.getInstance(this.getContext()).get(url, ofl);

--------------------------------------------------------------------------
----------------------------METHOD GET WITH PARAM-------------------------
--------------------------------------------------------------------------

OnFinishListener ofl = new OnFinishListener(){
    @Override
    public void onFinish( JSONObject o ) {
        //YOUR CODE HERE
    }
};
String url = getString(R.string.webservice) + "controller/method";

String ApiKey = this.getContext().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

Map<String, String> params = new HashMap<>();
params.put("key", ApiKey);

VolleyHelper.getInstance(this.getContext()).get(url, ofl, params);

--------------------------------------------------------------------------
---------------------------------METHOD POST------------------------------
--------------------------------------------------------------------------

OnFinishListener ofl = new OnFinishListener(){
    @Override
    public void onFinish( JSONObject o ) {
        //YOUR CODE HERE
    }
};
String url = getString(R.string.webservice) + "controller/method";

String ApiKey = this.getContext().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

Map<String, String> params = new HashMap<>();
params.put("key", ApiKey);

VolleyHelper.getInstance(this.getContext()).post(url, ofl, params);

--------------------------------------------------------------------------

--------------------------------------------------------------------------
try {
    String status = o.getString("status");
    //String message = o.getString("message");
    if(status.equalsIgnoreCase("true")){
        //JSONArray jsonArray = new JSONArray(o.getString("data"));
        //JSONObject jsonObject = new JSONObject(o.getString("data"));
    }
    else if(status.equalsIgnoreCase("false")){ //jika data 0 masuk sini juga
        //String message = o.getString("message");
        //Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
    }
    else{
        Log.e(getClass().getName(), "onFinish: " + o.toString());
        Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), o.toString(), Toast.LENGTH_LONG).show();
    }
}
catch(JSONException e){
    e.printStackTrace();
    Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
}
*/

public class VolleyHelper {
    private static VolleyHelper mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    public static synchronized VolleyHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyHelper(context);
        }
        return mInstance;
    }

    private VolleyHelper(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    private <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public String UriBuild(String url, Map<String, String> params){
        Uri.Builder builtUri = Uri.parse(url + "?").buildUpon();
        for(Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            builtUri.appendQueryParameter(key, value);
        }
        builtUri.build();
        return builtUri.toString();
    }

    public void get(String url, final OnFinishListener ofl) {
        StringRequest request = new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VolleyGet", response);
                        String status = "";
                        String message = "";
                        try{
                            JSONObject obj = new JSONObject(response);
                            ofl.onFinish(obj);
                        } catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            String json = new String(response.data);
                            Log.e("VolleyGetParam", json);
                            String message = json;
                            try{
                                JSONObject obj = new JSONObject(json);
                                message = obj.getString("message");
                            } catch(JSONException e){
                                e.printStackTrace();
                            }
                            if(json != null){
                                Toast.makeText(mCtx, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
        );

        addToRequestQueue(request);
    }

    public void get(String url, final OnFinishListener ofl, Map<String, String> params) {
        String buildUrl = UriBuild(url, params);

        StringRequest request = new StringRequest(
                Request.Method.GET, buildUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VolleyGetParam", response);
                        String status = "";
                        String message = "";
                        try{
                            JSONObject obj = new JSONObject(response);
                            ofl.onFinish(obj);
                        } catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            String json = new String(response.data);
                            Log.e("VolleyGet", json);
                            String message = json;
                            try{
                                JSONObject obj = new JSONObject(json);
                                message = obj.getString("message");
                            } catch(JSONException e){
                                e.printStackTrace();
                            }
                            if(json != null){
                                Toast.makeText(mCtx, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
        );

        addToRequestQueue(request);
    }

    public void post(String url, final OnFinishListener ofl, final Map<String, String> params) {
        final Map<String, String> finalParams = params;

        StringRequest request = new StringRequest(
                Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VolleyPost", response);
                        try{
                            JSONObject obj = new JSONObject(response);
                            ofl.onFinish(obj);
                        } catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            String json = new String(response.data);
                            Log.e("VolleyPost", json);
                            String message = json;
                            try{
                                JSONObject obj = new JSONObject(json);
                                message = obj.getString("message");
                            } catch(JSONException e){
                                e.printStackTrace();
                            }
                            if(json != null){
                                Toast.makeText(mCtx, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
        ) {
            //adding parameters to the request
            @Override
            protected Map<String, String> getParams() {
                return finalParams;
            }
        };

        addToRequestQueue(request);
    }
}
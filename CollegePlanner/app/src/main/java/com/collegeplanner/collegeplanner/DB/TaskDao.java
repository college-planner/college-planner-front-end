package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.TypeConverters;

@Dao
@TypeConverters(DateConverter.class)
public interface TaskDao {

}

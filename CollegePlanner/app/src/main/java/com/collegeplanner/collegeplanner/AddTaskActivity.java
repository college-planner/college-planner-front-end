package com.collegeplanner.collegeplanner;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class AddTaskActivity extends AppCompatActivity {

    TextView textName;
    TextView textDetail;
    TextView textDate;
    TextView textTime;
    Button buttonAddPicture;
    Button buttonAddTask;
    ImageView imageView;
    Spinner spinnerClass;

    Intent intent;
    Uri fileUri;
    Bitmap bitmap, decoded;
    public final int REQUEST_CAMERA = 0;
    public final int SELECT_FILE = 1;

    int bitmap_size = 40; // image quality 1 - 100;
    int max_resolution_image = 800;

    ArrayList<Map<String, String>> classes = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        textName = findViewById(R.id.at_textName);
        textDetail = findViewById(R.id.at_textDetail);
        textDate = findViewById(R.id.at_textDate);
        textTime = findViewById(R.id.at_textTime);
        buttonAddPicture = findViewById(R.id.at_buttonAddPicture);
        buttonAddTask = findViewById(R.id.at_buttonAddTask);
        imageView = findViewById(R.id.at_imageView);
        spinnerClass = findViewById(R.id.at_spinnerClass);

        textDate.setKeyListener(null);
        textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
            }
        });
        textDate.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus)openDatePicker();
            }
        });

        textTime.setKeyListener(null);
        textTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePicker();
            }
        });
        textTime.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) openTimePicker();
            }
        });


        buttonAddPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        buttonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String class_id = classes.get(spinnerClass.getSelectedItemPosition()).get("id");

                OnFinishListener ofl = new OnFinishListener(){
                    @Override
                    public void onFinish(JSONObject o ) {
                        try {
                            String status = o.getString("status");
                            String message = o.getString("message");
                            if (status.equalsIgnoreCase("true")) {
                                Toast.makeText(AddTaskActivity.this, message, Toast.LENGTH_SHORT).show();
                                finish();
                            } else if (status.equalsIgnoreCase("false")) {
                                Toast.makeText(AddTaskActivity.this, message, Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(AddTaskActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                                Toast.makeText(AddTaskActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                        catch(JSONException e){
                            e.printStackTrace();
                            Toast.makeText(AddTaskActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                };
                String url = getString(R.string.webservice) + "Task/insert_task";

                String ApiKey = getApplicationContext().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

                Map<String, String> params = new HashMap<>();
                params.put("key", ApiKey);
                params.put("class_id", class_id);
                params.put("name", textName.getText().toString());
                params.put("deadline", textDate.getText().toString() + " " + textTime.getText().toString());
                params.put("detail", textDetail.getText().toString());
                params.put("img", "");

                VolleyHelper.getInstance(getApplicationContext()).post(url, ofl, params);
            }
        });

        getAllClasses();
    }

    private void getAllClasses(){
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    if(status.equalsIgnoreCase("true")){
                        ArrayList<String> classesName = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(o.getString("data"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String semester_id = jsonObject.getString("semester_id");
                            String name = jsonObject.getString("name");
                            String detail = jsonObject.getString("detail");
                            String color = jsonObject.getString("color");
                            String gpa = jsonObject.getString("gpa");
                            String credit = jsonObject.getString("credit");
                            Map<String, String> dataList = new HashMap<>();
                            dataList.put("id", id);
                            dataList.put("semester_id", semester_id);
                            dataList.put("name", name);
                            dataList.put("detail", detail);
                            dataList.put("color", color);
                            dataList.put("gpa", gpa);
                            dataList.put("credit", credit);
                            classes.add(dataList);

                            classesName.add(name);
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddTaskActivity.this, android.R.layout.simple_spinner_item, classesName);

                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        spinnerClass.setAdapter(dataAdapter);
                    }
                    else if(status.equalsIgnoreCase("false")){
                        String message = o.getString("message");
                        Toast.makeText(AddTaskActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(AddTaskActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(AddTaskActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(AddTaskActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "Classes/allclass_user";

        String ApiKey = getApplication().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(getApplicationContext()).get(url, ofl, params);
    }

    private void openDatePicker(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DATE);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
            AddTaskActivity.this,
            new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.date_format));
                    String formatted = simpleDateFormat.format(calendar.getTime());
                    textDate.setText(formatted);
                }
            },
            year, month, day);
        datePickerDialog.show();
    }

    private void openTimePicker(){
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(
            AddTaskActivity.this,
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    textTime.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                }
            },
            hour, minute, true);
        timePickerDialog.show();
    }



    private void selectImage() {
        imageView.setImageResource(0);
        final CharSequence[] items = {"Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddTaskActivity.this);
        builder.setTitle("Add Photo!");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose from Library")) {
                    intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        View current = getCurrentFocus();
        if (current != null) current.clearFocus();
        Log.e("onActivityResult", "requestCode " + requestCode + ", resultCode " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                try {
                    Log.e("CAMERA", fileUri.getPath());

                    bitmap = BitmapFactory.decodeFile(fileUri.getPath());
                    setToImageView(getResizedBitmap(bitmap, max_resolution_image));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == SELECT_FILE && data != null && data.getData() != null) {
                try {
                    // mengambil gambar dari Gallery
                    bitmap = MediaStore.Images.Media.getBitmap(AddTaskActivity.this.getContentResolver(), data.getData());
                    setToImageView(getResizedBitmap(bitmap, max_resolution_image));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Untuk menampilkan bitmap pada ImageView
    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        imageView.setImageBitmap(decoded);
    }

    // Untuk resize bitmap
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DeKa");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("Monitoring", "Oops! Failed create Monitoring directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_DeKa_" + timeStamp + ".jpg");

        return mediaFile;
    }

}

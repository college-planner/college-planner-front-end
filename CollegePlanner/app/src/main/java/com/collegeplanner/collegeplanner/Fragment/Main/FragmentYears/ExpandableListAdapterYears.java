package com.collegeplanner.collegeplanner.Fragment.Main.FragmentYears;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExpandableListAdapterYears extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Map<String, String>> expandableListTitle;
    private ArrayList<Map<String, String>> expandableListDetail;

    public ExpandableListAdapterYears(Context context, ArrayList<Map<String, String>> expandableListTitle, ArrayList<Map<String, String>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Map<String, String> getChild(int groupId, int listPosition) {
        int count = 0;
        for (Map<String, String> entry : expandableListDetail) {
            if (entry.get("year_id").equalsIgnoreCase(expandableListTitle.get(groupId).get("id"))) {
                if (count == listPosition){
                    return entry;
                }
                else{
                    count++;
                }
            }
        }
        return null;
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return 0;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Map<String, String> expandedList = getChild(listPosition, expandedListPosition);
        if (expandedList != null) {
            String id = expandedList.get("id");
            String start = expandedList.get("start");
            String end = expandedList.get("end");
            convertView = layoutInflater.inflate(R.layout.fragment_year_expandablelistivew_item, null);
            TextView expandedListTextView = convertView.findViewById(R.id.fy_expandableListView_item);
            expandedListTextView.setText(start + " / " + end);
        }
        else{
            convertView = layoutInflater.inflate(R.layout.fragment_year_expandablelistivew_add_semester, null);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        int count = 1; //Add Semester

        for (Map<String, String> entry : expandableListDetail) {
            if (entry.get("year_id").equalsIgnoreCase(expandableListTitle.get(listPosition).get("id"))) {
                count++;
            }
        }

        return count;
    }

    @Override
    public Map<String, String> getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String id = getGroup(listPosition).get("id");
        String name = getGroup(listPosition).get("name");
        String gpa = getGroup(listPosition).get("gpa");
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.fragment_year_expandablelistivew_group, null);
        }
        TextView listTitleTextView = convertView.findViewById(R.id.fy_expandableListView_group);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(name + " (" + gpa + " GPA)");
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
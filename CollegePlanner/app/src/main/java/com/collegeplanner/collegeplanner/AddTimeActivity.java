package com.collegeplanner.collegeplanner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddTimeActivity extends AppCompatActivity {

    String class_id;

    Spinner spinner;
    TextView textLocation;
    TextView textStart;
    TextView textEnd;
    Button buttonSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_time);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null){
                class_id = extras.getString("class_id");
            }
        } else {
            class_id = savedInstanceState.getString("class_id");
        }

        spinner = findViewById(R.id.ati_spinner);
        textLocation = findViewById(R.id.ati_textLocation);
        textStart = findViewById(R.id.ati_textStart);
        textEnd = findViewById(R.id.ati_textEnd);
        buttonSubmit = findViewById(R.id.ati_buttonSubmit);

        ArrayList<String> daysOfWeek = new ArrayList<>();
        for (String day: new DateFormatSymbols().getWeekdays()){
            if (!day.isEmpty()) daysOfWeek.add(day);
        }
        ArrayAdapter<String> adapterDaysOfWeek = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, daysOfWeek);
        spinner.setAdapter(adapterDaysOfWeek);

        textStart.setKeyListener(null);
        textStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePickerStart();
            }
        });
        textStart.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus)openTimePickerStart();
            }
        });

        textEnd.setKeyListener(null);
        textEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePickerEnd();
            }
        });
        textEnd.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) openTimePickerEnd();
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInputValid()){
                    insertTime();
                }
            }
        });
    }

    private void openTimePickerStart(){
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(
                AddTimeActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        textStart.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                },
                hour, minute, true);
        timePickerDialog.show();
    }

    private void openTimePickerEnd(){
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(
                AddTimeActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        textEnd.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                    }
                },
                hour, minute, true);
        timePickerDialog.show();
    }

    private boolean checkInputValid(){
        boolean valid = true;
        if (textLocation.getText().toString().trim().isEmpty()){
            textLocation.setError(getString(R.string.error_field_required));
            textLocation.requestFocus();
            valid = false;
        }
        if (textStart.getText().toString().trim().isEmpty()){
            textStart.setError(getString(R.string.error_field_required));
            valid = false;
        }
        else{
            textStart.setError(null);
        }
        if (textEnd.getText().toString().trim().isEmpty()){
            textEnd.setError(getString(R.string.error_field_required));
            valid = false;
        }
        else{
            textEnd.setError(null);
        }
        return valid;
    }

    private void insertTime(){
        showProgress(true);
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    String message = o.getString("message");
                    if(status.equalsIgnoreCase("true")){
                        Toast.makeText(AddTimeActivity.this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else if(status.equalsIgnoreCase("false")){ //jika data 0 masuk sini juga
                        Toast.makeText(AddTimeActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(AddTimeActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(AddTimeActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(AddTimeActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "Time/insert_time";

        String ApiKey = getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);
        params.put("class_id", class_id);
        params.put("days", String.valueOf(spinner.getSelectedItemPosition() + 1));
        params.put("location", textLocation.getText().toString());
        params.put("start", textStart.getText().toString());
        params.put("end", textEnd.getText().toString());


        VolleyHelper.getInstance(this).post(url, ofl, params);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        final View fragmentView = this.findViewById(R.id.ati_form);
        final View progressView = this.findViewById(R.id.ati_progressBar);

        InputMethodManager imm =  (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            fragmentView.setVisibility(show ? View.GONE : View.VISIBLE);
            fragmentView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    fragmentView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            fragmentView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

package com.collegeplanner.collegeplanner.Fragment.Main.FragmentYears;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.AddSemesterActivity;
import com.collegeplanner.collegeplanner.AddYearActivity;
import com.collegeplanner.collegeplanner.Fragment.Main.AbstractFragmentMain;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentClasses;
import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FragmentYears extends AbstractFragmentMain {
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;

    TextView textGpa;
    TextView textCredit;

    ArrayList<Map<String, String>> group = new ArrayList<>();
    ArrayList<Map<String, String>> item = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_years, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        textGpa = view.findViewById(R.id.fy_textGpa);
        textCredit = view.findViewById(R.id.fy_textCredit);
        expandableListView = view.findViewById(R.id.fy_expandableListView);

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (parent.getExpandableListAdapter().getChildrenCount(groupPosition) == childPosition + 1){
                    String idYear = group.get(groupPosition).get("id");
                    Intent i = new Intent(getActivity(), AddSemesterActivity.class);
                    i.putExtra("idYear", idYear);
                    startActivity(i);
                }
                else{
                    FragmentClasses fragmentClassesMain = new FragmentClasses();
                    //Bundle bundle = new Bundle();
                    //bundle.putString("class_id", class_id);
                    //fragmentClassesMain.setArguments(bundle);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_detail, fragmentClassesMain);
                    ft.commit();
                }

                return false;
            }
        });


        FloatingActionButton fab = view.findViewById(R.id.fab_add_years);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddYearActivity.class);
                startActivity(i);

            }
        });

        showProgress(true);
        getGpaCredit(view);
        getExpandableYears(view);
    }

    private void getExpandableYears(final View view) {
        String url = getString(R.string.webservice) + "Year/allyear";

        final String ApiKey = this.getContext().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = new JSONArray(o.getString("data"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String gpa = jsonObject.getString("gpa");
                            String name = jsonObject.getString("name");

                            Map<String, String> dataList = new HashMap<>();
                            dataList.put("id", id);
                            dataList.put("gpa", gpa);
                            dataList.put("name", name);

                            group.add(dataList);
                        }

                        OnFinishListener ofl = new OnFinishListener() {
                            @Override
                            public void onFinish(JSONObject o) {
                                try {
                                    String status = o.getString("status");
                                    if (status.equalsIgnoreCase("true")) {
                                        JSONArray jsonArray = new JSONArray(o.getString("data"));
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                                            String id = jsonObject.getString("id");
                                            String start = jsonObject.getString("start");
                                            String end = jsonObject.getString("end");
                                            String year_id = jsonObject.getString("year_id");

                                            Map<String, String> dataList = new HashMap<>();
                                            dataList.put("id", id);
                                            dataList.put("start", start);
                                            dataList.put("end", end);
                                            dataList.put("year_id", year_id);
                                            item.add(dataList);
                                        }
                                    }
                                    else if(status.equalsIgnoreCase("false")){

                                    }
                                    else{
                                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                                        Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                                        Toast.makeText(getContext(), o.toString(), Toast.LENGTH_LONG).show();
                                    }

                                    expandableListAdapter = new ExpandableListAdapterYears(view.getContext(), group, item);
                                    expandableListView.setAdapter(expandableListAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                                }
                            }
                        };

                        String url = getString(R.string.webservice) + "Semester/allsemester_user";

                        Map<String, String> params = new HashMap<>();
                        params.put("key", ApiKey);

                        VolleyHelper.getInstance(view.getContext()).get(url, ofl, params);

                    }
                    else if(status.equalsIgnoreCase("false")){

                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getContext(), o.toString(), Toast.LENGTH_LONG).show();
                    }
                    showProgress(false);
                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        };


        VolleyHelper.getInstance(this.getContext()).get(url, ofl, params);
    }

    private void getGpaCredit(final View view){
        OnFinishListener ofl = new OnFinishListener() {
            @Override
            public void onFinish(JSONObject o) {
                try {
                    String status = o.getString("status");
                    if(status.equalsIgnoreCase("true")) {
                        JSONObject data = new JSONObject(o.getString("data"));
                        textGpa.setText(data.getString("gpa"));
                        textCredit.setText(data.getString("credit"));
                    }else if(status.equalsIgnoreCase("false")){

                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getContext(), o.toString(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };

        String url = getString(R.string.webservice) + "Year/year_header";

        final String ApiKey = this.getContext().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");
        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(view.getContext()).get(url, ofl, params);
    }


}

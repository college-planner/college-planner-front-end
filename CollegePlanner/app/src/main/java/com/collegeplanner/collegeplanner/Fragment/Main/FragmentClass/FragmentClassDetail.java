package com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.collegeplanner.collegeplanner.ClassActivity;
import com.collegeplanner.collegeplanner.R;

import java.util.Map;

public class FragmentClassDetail extends Fragment implements ClassActivity.OnAboutDataReceivedListener {
    TextView textDetail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_class_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ((ClassActivity)getActivity()).setAboutDataListener(this);

        textDetail = view.findViewById(R.id.cde_textDetail);
    }

    @Override
    public void onDataReceived(Object data) {
        String detail = String.valueOf(data);
        if (detail.isEmpty()){
            textDetail.setText("Nothing here. Tap edit to add some details.");
        }
        else{
            textDetail.setText(detail);
        }
    }
}

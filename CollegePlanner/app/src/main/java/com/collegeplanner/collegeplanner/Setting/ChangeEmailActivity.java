package com.collegeplanner.collegeplanner.Setting;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangeEmailActivity extends AppCompatActivity {

    TextView textOld;
    TextView textNew;
    Button buttonSubmit;

    View progressView;
    View formView;

    String oldData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_email);

        progressView = findViewById(R.id.ce_progress);
        formView = findViewById(R.id.ce_form);

        textOld = findViewById(R.id.ce_textOldEmail);
        textNew = findViewById(R.id.ce_textNewEmail);
        buttonSubmit = findViewById(R.id.ce_buttonSubmit);

        oldData = null;
        getOld();

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptChange();
            }
        });
    }

    private void attemptChange(){
        boolean cancel = false;

        String _new = textNew.getText().toString().trim().toLowerCase();
        String _old = textOld.getText().toString().trim().toLowerCase();

        if (_old.isEmpty()){
            textOld.setError(getString(R.string.error_field_required));
            textOld.requestFocus();
            cancel = true;
        }
        else if (_new.isEmpty()){
            textNew.setError(getString(R.string.error_field_required));
            textNew.requestFocus();
            cancel = true;
        }
        else if(!_old.contains("@")){
            textOld.setError(getString(R.string.error_invalid_email));
            textOld.requestFocus();
            cancel = true;
        }
        else if(!_old.contains("@")){
            textNew.setError(getString(R.string.error_invalid_email));
            textNew.requestFocus();
            cancel = true;
        }
        else if (_new.equals(_old)){
            textNew.setError("New email must not be same with old email");
            textNew.requestFocus();
            cancel = true;
        }
        else if (oldData == null){
            Snackbar.make(findViewById(R.id.ce_coordinatorLayout), "Unknown error. Please try again", Snackbar.LENGTH_SHORT).show();
            cancel = true;
        }
        else if (!oldData.equals(_old)){
            textOld.setError("This email do not match with old email");
            textOld.requestFocus();
            cancel = true;
        }


        if (!cancel){
            showProgress(true);
            OnFinishListener ofl = new OnFinishListener(){
                @Override
                public void onFinish( JSONObject o ) {
                    try {
                        String status = o.getString("status");
                        String message = o.getString("message");
                        if(status.equalsIgnoreCase("true")){
                            Toast.makeText(ChangeEmailActivity.this, message, Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else if(status.equalsIgnoreCase("false")){
                            if (message.toLowerCase().contains("email") && message.toLowerCase().contains("used")){
                                textNew.setError(message);
                                textNew.requestFocus();
                            }
                            else{
                                Snackbar.make(findViewById(R.id.ce_coordinatorLayout), message, Snackbar.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Log.e(getClass().getName(), "onFinish: " + o.toString());
                            Snackbar.make(findViewById(R.id.ce_coordinatorLayout), "Oops, something wrong", Snackbar.LENGTH_SHORT).show();
                            Toast.makeText(ChangeEmailActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                        }
                        showProgress(false);
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                        Snackbar.make(findViewById(R.id.ce_coordinatorLayout), "Oops, something wrong", Snackbar.LENGTH_SHORT).show();
                    }
                }
            };
            String url = getString(R.string.webservice) + "User/change_email";

            String ApiKey = getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

            Map<String, String> params = new HashMap<>();
            params.put("key", ApiKey);
            params.put("old_email", _old);
            params.put("new_email", _new);

            VolleyHelper.getInstance(getApplicationContext()).post(url, ofl, params);
        }
    }

    private void getOld(){
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    //String message = o.getString("message");
                    if(status.equalsIgnoreCase("true")){
                        JSONObject jsonObject = new JSONObject(o.getString("data"));
                        oldData = jsonObject.getString("email");
                    }
                    else if(status.equalsIgnoreCase("false")){ //jika data 0 masuk sini juga
                        //String message = o.getString("message");
                        //Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(ChangeEmailActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ChangeEmailActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(ChangeEmailActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "User/user";

        String ApiKey = this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(this).get(url, ofl, params);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

package com.collegeplanner.collegeplanner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddYearActivity extends AppCompatActivity {

    EditText ed_year;
    Button btnAdd;
    Activity act;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_year);
        ed_year = findViewById(R.id.ay_textYearName);
        btnAdd = findViewById(R.id.ay_buttonAddYear);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean cancel = false;
                if (ed_year.getText().toString().trim().isEmpty()){
                    ed_year.setError(getString(R.string.error_field_required));
                    ed_year.requestFocus();
                    cancel = true;
                }
                if (!cancel){
                    OnFinishListener ofl = new OnFinishListener(){
                        @Override
                        public void onFinish( JSONObject o ) {
                            try{
                                String status = o.getString("status");
                                String message = o.getString("message");
                                if(status.equalsIgnoreCase("true")){
                                    Toast.makeText(AddYearActivity.this, message, Toast.LENGTH_SHORT).show();

                                    Intent i = new Intent(AddYearActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                                else if(status.equalsIgnoreCase("false")){
                                    Toast.makeText(AddYearActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Log.e(getClass().getName(), "onFinish: " + o.toString());
                                    Toast.makeText(AddYearActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                                    Toast.makeText(AddYearActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                                }
                            } catch(JSONException e){
                                e.printStackTrace();
                                Toast.makeText(AddYearActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    };
                    String url = getString(R.string.webservice) + "Year/insert_year";

                    final String ApiKey = AddYearActivity.this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");
                    Map<String, String> params = new HashMap<>();
                    params.put("key", ApiKey);
                    params.put("name", ed_year.getText().toString());

                    VolleyHelper.getInstance(AddYearActivity.this).post(url, ofl, params);
                }
            }
        });

    }
}

package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "semester", foreignKeys = @ForeignKey(entity = Year.class,
        parentColumns = "id",
        childColumns = "year_id"))
@TypeConverters(DateConverter.class)
public class Semester {
    @PrimaryKey
    public int id;

    public int year_id;

    public Double gpa;

    public Date start;

    public Date end;
}
package com.collegeplanner.collegeplanner.Helper;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.collegeplanner.collegeplanner.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class AdapterRecyclerClassTask extends RecyclerView.Adapter<AdapterRecyclerClassTask.ViewHolder> {
    private RecyclerViewClickListener mListener;
    private Context mCtx;
    ArrayList<Map<String, String>> data;

    public AdapterRecyclerClassTask(Context context, RecyclerViewClickListener listener) {
        data = new ArrayList<>();
        mCtx = context;
        mListener = listener;
    }

    public void updateData(ArrayList<Map<String, String>> dataset) {
        data = dataset;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_class_task, parent, false);
        return new ViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cardView.setCardBackgroundColor(Color.parseColor("#" + data.get(position).get("class_color")));
        holder.textTitle.setText(data.get(position).get("name"));
        holder.textSubtitle.setText(data.get(position).get("class_name") + " - due " + data.get(position).get("deadline_date") + " "+ data.get(position).get("deadline_time"));
        holder.textDetail.setText(data.get(position).get("detail"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RecyclerViewClickListener mListener;
        CardView cardView;
        TextView textTitle;
        TextView textSubtitle;
        TextView textDetail;

        public ViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);

            cardView = itemView.findViewById(R.id.rcta_cardView);
            textTitle = itemView.findViewById(R.id.rcta_textTitle);
            textSubtitle = itemView.findViewById(R.id.rcta_textSubtitle);
            textDetail = itemView.findViewById(R.id.rcta_textDetail);

            mListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {
        void onClick(View view, int position);
    }
}

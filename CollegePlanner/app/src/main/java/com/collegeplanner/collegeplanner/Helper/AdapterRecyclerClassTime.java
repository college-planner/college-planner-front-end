package com.collegeplanner.collegeplanner.Helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.collegeplanner.collegeplanner.ClassActivity;
import com.collegeplanner.collegeplanner.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class AdapterRecyclerClassTime extends RecyclerView.Adapter<AdapterRecyclerClassTime.ViewHolder> {
    private RecyclerViewClickListener mListener;
    private Context mCtx;
    ArrayList<Map<String, String>> data;

    public AdapterRecyclerClassTime(Context context, RecyclerViewClickListener listener) {
        data = new ArrayList<>();
        mCtx = context;
        mListener = listener;
    }

    public void updateData(ArrayList<Map<String, String>> dataset) {
        data = dataset;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_class_time, parent, false);
        return new ViewHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, 6, Integer.valueOf(data.get(position).get("days")));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(mCtx.getString(R.string.date_format_name_of_day));
        String formatted = simpleDateFormat.format(calendar.getTime());

        holder.cardView.setCardBackgroundColor(Color.parseColor("#" + data.get(position).get("class_color")));
        holder.textDay.setText(formatted);
        holder.textLocation.setText(data.get(position).get("location"));
        holder.textTime.setText(data.get(position).get("start") + " - " + data.get(position).get("end"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RecyclerViewClickListener mListener;
        CardView cardView;
        TextView textDay;
        TextView textLocation;
        TextView textTime;

        public ViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);

            cardView = itemView.findViewById(R.id.rcti_cardView);
            textDay = itemView.findViewById(R.id.rcti_textDay);
            textLocation = itemView.findViewById(R.id.rcti_textLocation);
            textTime = itemView.findViewById(R.id.rcti_textTime);

            mListener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {
        void onClick(View view, int position);
    }
}

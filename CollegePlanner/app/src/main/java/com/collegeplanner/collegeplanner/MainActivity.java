package com.collegeplanner.collegeplanner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.collegeplanner.collegeplanner.Fragment.Main.FragmentAgenda;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentCalendar;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentClasses;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentDay;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentSchedule;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentSetting;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentYears.FragmentYears;
import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar;
    TextView textName;
    TextView textEmail;
    View fragmentView;
    View progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        fragmentView = findViewById(R.id.fragment_detail);
        progressView = findViewById(R.id.fragment_progressBar);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_detail, new FragmentCalendar(), "Calendar");
        ft.commit();

        View headerView = navigationView.getHeaderView(0);

        textName = headerView.findViewById(R.id.main_textName);
        textEmail = headerView.findViewById(R.id.main_textEmail);

        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    JSONObject data = new JSONObject(o.getString("data"));
                    textName.setText(data.getString("firstname") + " " + data.getString("lastname"));
                    textEmail.setText(data.getString("email"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        String url = getString(R.string.webservice) + "user/user";

        String ApiKey = this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(this).get(url, ofl, params);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (id == R.id.nav_calendar) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentCalendar(), "Calendar");
        } else if (id == R.id.nav_day) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentDay());
        } else if (id == R.id.nav_agenda) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentAgenda());
        } else if (id == R.id.nav_schedule) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentSchedule());
        } else if (id == R.id.nav_classes) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentClasses());
        } else if (id == R.id.nav_year) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentYears());
        } else if (id == R.id.nav_logout) {
            SharedPreferences prefApiKey = getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor prefApiKeyEditor = prefApiKey.edit();
            prefApiKeyEditor.putString(getString(R.string.preference_api_key), null);
            prefApiKeyEditor.commit();

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_setting) {
            fragmentTransaction.replace(R.id.fragment_detail, new FragmentSetting());
        }

        fragmentTransaction.commit();

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            fragmentView.setVisibility(show ? View.GONE : View.VISIBLE);
            fragmentView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    fragmentView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            fragmentView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}

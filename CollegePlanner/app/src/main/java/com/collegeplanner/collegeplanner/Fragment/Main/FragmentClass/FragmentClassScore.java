package com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.collegeplanner.collegeplanner.ClassActivity;
import com.collegeplanner.collegeplanner.R;

public class FragmentClassScore extends Fragment implements ClassActivity.OnAboutDataReceivedListener{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_class_score, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        
    }

    @Override
    public void onDataReceived(Object data) {

    }
}

package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

@Database(entities = {
        User.class,
        Year.class,
        Semester.class,
        Class.class,
        Task.class,
        Score.class,
        Time.class
    }, version = 1)
public abstract class MyDatabase extends RoomDatabase {
    private static MyDatabase INSTANCE;

    public abstract UserDao userModel();

    public abstract YearDao bookModel();

    public abstract SemesterDao semesterModel();

    public abstract ClassDao classModel();

    public abstract TaskDao taskModel();

    public abstract ScoreDao scoreModel();

    public abstract TimeDao timeModel();

    public static MyDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.inMemoryDatabaseBuilder(context.getApplicationContext(), MyDatabase.class)
                            // To simplify the codelab, allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}

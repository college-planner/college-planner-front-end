package com.collegeplanner.collegeplanner.Setting;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity {

    TextView textOldPassword;
    TextView textNewPassword;
    TextView textNewPasswordConfirm;
    Button buttonSubmit;

    View progressView;
    View formView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        textOldPassword = findViewById(R.id.cp_textOldPassword);
        textNewPassword = findViewById(R.id.cp_textNewPassword);
        textNewPasswordConfirm = findViewById(R.id.cp_textNewPasswordConfirm);
        buttonSubmit = findViewById(R.id.cp_buttonSubmit);

        progressView = findViewById(R.id.cp_progress);
        formView = findViewById(R.id.cp_form);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptChange();
            }
        });
    }

    private void attemptChange(){
        boolean cancel = false;

        String _old = textOldPassword.getText().toString();
        String _new = textNewPassword.getText().toString();
        String _newconfirm = textNewPasswordConfirm.getText().toString();

        if (_old.isEmpty()){
            textOldPassword.setError(getString(R.string.error_field_required));
            textOldPassword.requestFocus();
            cancel = true;
        }
        else if (_new.isEmpty()){
            textNewPassword.setError(getString(R.string.error_field_required));
            textNewPassword.requestFocus();
            cancel = true;
        }
        else if(_newconfirm.isEmpty()){
            textNewPasswordConfirm.setError(getString(R.string.error_field_required));
            textNewPasswordConfirm.requestFocus();
            cancel = true;
        }
        else if(!_new.equals(_newconfirm)){
            textNewPasswordConfirm.setError(getString(R.string.error_confirm_password));
            textNewPasswordConfirm.requestFocus();
            cancel = true;
        }

        if (!cancel){
            showProgress(true);
            OnFinishListener ofl = new OnFinishListener(){
                @Override
                public void onFinish( JSONObject o ) {
                    try {
                        String status = o.getString("status");
                        String message = o.getString("message");
                        if(status.equalsIgnoreCase("true")){
                            Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else if(status.equalsIgnoreCase("false")){
                            if(message.toLowerCase().contains("wrong") && message.toLowerCase().contains("wrong")){
                                textOldPassword.setError(message);
                            }
                            else{
                                Snackbar.make(findViewById(R.id.cp_coordinatorLayout), message, Snackbar.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Log.e(getClass().getName(), "onFinish: " + o.toString());
                            Snackbar.make(findViewById(R.id.cp_coordinatorLayout), "Oops, something wrong", Snackbar.LENGTH_SHORT).show();
                            Toast.makeText(ChangePasswordActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                        }
                        showProgress(false);
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                        Snackbar.make(findViewById(R.id.cp_coordinatorLayout), "Oops, something wrong", Snackbar.LENGTH_SHORT).show();
                    }
                }
            };
            String url = getString(R.string.webservice) + "User/change_password";

            String ApiKey = getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

            Map<String, String> params = new HashMap<>();
            params.put("key", ApiKey);
            params.put("oldpassword", _old);
            params.put("newpassword", _new);

            VolleyHelper.getInstance(getApplicationContext()).post(url, ofl, params);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

package com.collegeplanner.collegeplanner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText firstname, lastname, password, confpassword;
    AutoCompleteTextView email;
    Button button_register, login;

    private View mProgressView;
    private View mRegisterFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_activity_register);
        setContentView(R.layout.activity_register);

        firstname = findViewById(R.id.edFirstName);
        lastname = findViewById(R.id.edLastName);
        email = findViewById(R.id.edEmail);
        password = findViewById(R.id.edPassword);
        confpassword = findViewById(R.id.edConf);
        button_register = findViewById(R.id.btnRegister);
        login = findViewById(R.id.go_login);

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginActivity = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(loginActivity);
                finish();
            }
        });

        mRegisterFormView = findViewById(R.id.register_form);
        mProgressView = findViewById(R.id.register_progress);
    }

    private void attemptRegister() {
        email.setError(null);
        password.setError(null);
        firstname.setError(null);
        lastname.setError(null);
        confpassword.setError(null);

        String tempEmail = email.getText().toString();
        String tempPassword = password.getText().toString();
        String tempFirstName = firstname.getText().toString();
        String tempLastName = lastname.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(tempPassword) || !isInputValid(tempPassword)) {
            password.setError(getString(R.string.error_invalid_password));
            focusView = password;
            cancel = true;
        }
        else if (!password.getText().toString().equals(confpassword.getText().toString())) {
            confpassword.setError(getString(R.string.error_confirm_password));
            focusView = confpassword;
            cancel = true;
        }
        if (TextUtils.isEmpty(tempFirstName) || !isInputValid(tempFirstName)) {
            firstname.setError(getString(R.string.error_invalid_name));
            focusView = firstname;
            cancel = true;
        }
        if (TextUtils.isEmpty(tempLastName) || !isInputValid(tempLastName)) {
            lastname.setError(getString(R.string.error_invalid_name));
            focusView = firstname;
            cancel = true;
        }
        if (TextUtils.isEmpty(tempEmail)) {
            email.setError(getString(R.string.error_field_required));
            focusView = email;
            cancel = true;
        }
        else if (!isEmailValid(tempEmail)) {
            email.setError(getString(R.string.error_invalid_email));
            focusView = email;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);

            OnFinishListener ofl = new OnFinishListener(){
                @Override
                public void onFinish( JSONObject o ) {
                    try {
                        String status = o.getString("status");
                        String message = o.getString("message");
                        if(status.equalsIgnoreCase("true")){
                            Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        else if(status.equalsIgnoreCase("false")){
                            Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Log.e(getClass().getName(), "onFinish: " + o.toString());
                            Toast.makeText(RegisterActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                            Toast.makeText(RegisterActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                        }
                        showProgress(false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(RegisterActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                    }

                }
            };
            String url = getString(R.string.webservice) + "user/register";

            Map<String, String> params = new HashMap<>();
            params.put("email", email.getText().toString());
            params.put("password", password.getText().toString());
            params.put("firstname", firstname.getText().toString());
            params.put("lastname", lastname.getText().toString());

            VolleyHelper.getInstance(this).post(url, ofl, params);
        }
    }

    private boolean isInputValid(String word) {
        return word.length() > 0;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegisterFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

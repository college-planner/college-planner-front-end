package com.collegeplanner.collegeplanner.Helper;

import org.json.JSONException;
import org.json.JSONObject;

public interface OnFinishListener {
    public void onFinish( JSONObject o );
}

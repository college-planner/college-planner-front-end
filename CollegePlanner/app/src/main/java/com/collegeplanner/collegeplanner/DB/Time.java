package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

@Entity(tableName = "time", foreignKeys = @ForeignKey(entity = Class.class,
        parentColumns = "id",
        childColumns = "class_id"))
@TypeConverters(DateConverter.class)
public class Time {
    @PrimaryKey
    public int id;

    public int class_id;

    public int days;

    public String location;

    public Date start;

    public Date end;
}
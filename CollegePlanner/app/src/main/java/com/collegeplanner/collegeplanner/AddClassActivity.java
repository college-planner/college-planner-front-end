package com.collegeplanner.collegeplanner;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Fragment.Main.FragmentYears.ExpandableListAdapterYears;
import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import petrov.kristiyan.colorpicker.ColorPicker;

public class AddClassActivity extends AppCompatActivity {
    TextView textName;
    TextView textDetail;
    TextView textCredit;
    TextView color;
    TextView textColor;
    CardView cardViewColor;
    Spinner spinnerSemester;
    Button buttonAddClass;

    ArrayList<Map<String, String>> semesters = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);

        textName = findViewById(R.id.ac_textName);
        textDetail = findViewById(R.id.ac_textDetail);
        textCredit = findViewById(R.id.ac_textCredit);
        color = findViewById(R.id.ac_color);
        textColor = findViewById(R.id.ac_textColor);
        cardViewColor = findViewById(R.id.ac_cardViewColor);
        spinnerSemester = findViewById(R.id.ac_spinnerSemester);
        buttonAddClass = findViewById(R.id.ac_buttonAddClass);

        cardViewColor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openColorPicker();
            }
        });

        buttonAddClass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                boolean cancel = false;
                if (textName.getText().toString().trim().isEmpty()){
                    textName.setError(getString(R.string.error_field_required));
                    textName.requestFocus();
                    cancel = true;
                }
                if (textDetail.getText().toString().trim().isEmpty()){
                    textDetail.setError(getString(R.string.error_field_required));
                    textDetail.requestFocus();
                    cancel = true;
                }
                if (textCredit.getText().toString().trim().isEmpty()){
                    textCredit.setError(getString(R.string.error_field_required));
                    textCredit.requestFocus();
                    cancel = true;
                }
                if (!textCredit.getText().toString().trim().matches("[0-9]+")){
                    textCredit.setError(getString(R.string.error_field_numeric_only));
                    textCredit.requestFocus();
                    cancel = true;
                }


                if (!cancel){
                    addClass();
                }
            }
        });

        color.setBackgroundColor(Color.parseColor("#008B8B"));
        getAllSemester();
    }

    private void getAllSemester(){
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    if(status.equalsIgnoreCase("true")){
                        ArrayList<String> classesName = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(o.getString("data"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String year_id = jsonObject.getString("year_id");
                            String gpa = jsonObject.getString("gpa");
                            String start = jsonObject.getString("start");
                            String end = jsonObject.getString("end");
                            Map<String, String> dataList = new HashMap<>();
                            dataList.put("id", id);
                            dataList.put("year_id", year_id);
                            dataList.put("gpa", gpa);
                            dataList.put("start", start);
                            dataList.put("end", end);
                            semesters.add(dataList);

                            String semesterName = start + " - "  + end;
                            classesName.add(semesterName);
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddClassActivity.this, android.R.layout.simple_spinner_item, classesName);

                        // Drop down layout style - list view with radio button
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        spinnerSemester.setAdapter(dataAdapter);
                    }
                    else if(status.equalsIgnoreCase("false")){
                        String message = o.getString("message");
                        Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(AddClassActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(AddClassActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(AddClassActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "Semester/allsemester_user";

        String ApiKey = getApplication().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(getApplicationContext()).get(url, ofl, params);
    }

    private void openColorPicker(){
        ColorPicker colorPicker = new ColorPicker(this);

        colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                    @Override
                    public void onChooseColor(int position, int colorValue) {
                        if (colorValue != 0){
                            color.setBackgroundColor(colorValue);

                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                }).show();
    }

    private void addClass(){
        int colorCode = 0;
        if (color.getBackground() instanceof ColorDrawable) {
            ColorDrawable cd = (ColorDrawable) color.getBackground();
            colorCode = cd.getColor();
        }

        String hexColor = String.format("%06X", (0xFFFFFF & colorCode));
        String semester_id = semesters.get(spinnerSemester.getSelectedItemPosition()).get("id");

        String url = getString(R.string.webservice) + "Classes/insert_class";

        final String ApiKey = this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);
        params.put("semester_id", semester_id);
        params.put("name", textName.getText().toString());
        params.put("detail", textDetail.getText().toString());
        params.put("color", hexColor);
        params.put("credit", textCredit.getText().toString());

        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    String message = o.getString("message");
                    if(status.equalsIgnoreCase("true")){
                        Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else if(status.equalsIgnoreCase("false")){
                        Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(AddClassActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(AddClassActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(AddClassActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };


        VolleyHelper.getInstance(this).post(url, ofl, params);
    }
}

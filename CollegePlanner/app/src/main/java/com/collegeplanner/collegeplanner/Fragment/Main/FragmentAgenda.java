package com.collegeplanner.collegeplanner.Fragment.Main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.collegeplanner.collegeplanner.AddTaskActivity;
import com.collegeplanner.collegeplanner.R;

import java.util.ArrayList;
import java.util.Map;

public class FragmentAgenda extends AbstractFragmentMain {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    com.github.clans.fab.FloatingActionButton fabAddTask;
    ArrayList<Map<String, String>> testData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.fragment_agenda, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        fabAddTask = view.findViewById(R.id.fc_fab_add_task);
        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddTaskActivity.class);
                startActivity(i);
            }
        });


        /*mRecyclerView = view.findViewById(R.id.fc_recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        testData = new ArrayList<Map<String, String>>();
        Map<String, String> testDataSatu = new HashMap<String, String>();
        testDataSatu.put("data", "Agenda 1");
        testDataSatu.put("color", "00FF00");
        testData.add(testDataSatu);
        // specify an adapter (see also next example)
        mAdapter = new AdapterRecyclerClasses(testData);
        mRecyclerView.setAdapter(mAdapter);*/
    }
}

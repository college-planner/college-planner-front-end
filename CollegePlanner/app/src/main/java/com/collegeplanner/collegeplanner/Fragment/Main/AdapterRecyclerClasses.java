package com.collegeplanner.collegeplanner.Fragment.Main;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.collegeplanner.collegeplanner.R;

import java.util.ArrayList;
import java.util.Map;

public class AdapterRecyclerClasses extends RecyclerView.Adapter<AdapterRecyclerClasses.RecycleClassHolder> {
    private RecyclerViewClickListener mListener;
    ArrayList<Map<String, String>> data;

    public AdapterRecyclerClasses(RecyclerViewClickListener listener) {
        data = new ArrayList<>();
        mListener = listener;
    }

    public void updateData(ArrayList<Map<String, String>> dataset) {
        data.clear();
        data.addAll(dataset);
        notifyDataSetChanged();
    }


    @Override
    public RecycleClassHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_classes, parent, false);
        return new RecycleClassHolder(v, mListener);
    }

    @Override
    public void onBindViewHolder(RecycleClassHolder holder, int position) {
        holder.constraintLayout.setBackgroundColor(Color.parseColor("#" + data.get(position).get("color")));
        holder.text.setText(data.get(position).get("data"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecycleClassHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView text;
        ConstraintLayout constraintLayout;

        private RecyclerViewClickListener mListener;

        public RecycleClassHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            text = itemView.findViewById(R.id.rc_text);
            constraintLayout = itemView.findViewById(R.id.rc_layout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {
        void onClick(View view, int position);
    }
}
package com.collegeplanner.collegeplanner.Setting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.collegeplanner.collegeplanner.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_about);
    }
}

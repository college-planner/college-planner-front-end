package com.collegeplanner.collegeplanner.Setting;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangeNameActivity extends AppCompatActivity {
    TextView textFirstName;
    TextView textLastName;
    Button buttonSubmit;

    View progressView;
    View formView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);

        textFirstName = findViewById(R.id.cn_textFirstName);
        textLastName = findViewById(R.id.cn_textLastName);
        buttonSubmit = findViewById(R.id.cn_buttonSubmit);

        progressView = findViewById(R.id.cn_progress);
        formView = findViewById(R.id.cn_form);

        showProgress(true);
        getOld();

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptChange();
            }
        });
    }

    private void attemptChange(){
        boolean cancel = false;

        String _first = textFirstName.getText().toString().trim().toLowerCase();
        String _last = textLastName.getText().toString().trim().toLowerCase();

        if (_first.isEmpty()){
            textFirstName.setError(getString(R.string.error_field_required));
            textFirstName.requestFocus();
            cancel = true;
        }
        else if (_last.isEmpty()){
            textLastName.setError(getString(R.string.error_field_required));
            textLastName.requestFocus();
            cancel = true;
        }

        if (!cancel){
            showProgress(true);
            OnFinishListener ofl = new OnFinishListener(){
                @Override
                public void onFinish( JSONObject o ) {
                    try {
                        String status = o.getString("status");
                        String message = o.getString("message");
                        if(status.equalsIgnoreCase("true")){
                            Toast.makeText(ChangeNameActivity.this, message, Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else if(status.equalsIgnoreCase("false")){
                            Snackbar.make(findViewById(R.id.cn_coordinatorLayout), message, Snackbar.LENGTH_SHORT).show();
                        }
                        else{
                            Log.e(getClass().getName(), "onFinish: " + o.toString());
                            Snackbar.make(findViewById(R.id.cn_coordinatorLayout), "Oops, something wrong", Snackbar.LENGTH_SHORT).show();
                            Toast.makeText(ChangeNameActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                        }
                        showProgress(false);
                    }
                    catch(JSONException e){
                        e.printStackTrace();
                        Snackbar.make(findViewById(R.id.cn_coordinatorLayout), "Oops, something wrong", Snackbar.LENGTH_SHORT).show();
                    }
                }
            };
            String url = getString(R.string.webservice) + "User/change_name";

            String ApiKey = getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

            Map<String, String> params = new HashMap<>();
            params.put("key", ApiKey);
            params.put("firstname", _first);
            params.put("lastname", _last);

            VolleyHelper.getInstance(getApplicationContext()).post(url, ofl, params);
        }
    }


    private void getOld(){
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    //String message = o.getString("message");
                    if(status.equalsIgnoreCase("true")){
                        JSONObject jsonObject = new JSONObject(o.getString("data"));
                        textFirstName.setText(jsonObject.getString("firstname"));
                        textLastName.setText(jsonObject.getString("lastname"));
                        showProgress(false);
                    }
                    else if(status.equalsIgnoreCase("false")){ //jika data 0 masuk sini juga
                        String message = o.getString("message");
                        Toast.makeText(ChangeNameActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(ChangeNameActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ChangeNameActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(ChangeNameActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "User/user";

        String ApiKey = this.getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(this).get(url, ofl, params);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

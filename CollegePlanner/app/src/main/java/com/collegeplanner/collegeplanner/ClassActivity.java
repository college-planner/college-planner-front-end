package com.collegeplanner.collegeplanner;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import android.widget.Toolbar;

import com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass.FragmentClassDetail;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass.FragmentClassScore;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass.FragmentClassTask;
import com.collegeplanner.collegeplanner.Fragment.Main.FragmentClass.FragmentClassTime;
import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassActivity extends AppCompatActivity {

    String class_id;
    String class_name;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    FragmentClassDetail fragmentClassDetail;
    FragmentClassTime fragmentClassTime;
    FragmentClassTask fragmentClassTask;
    FragmentClassScore fragmentClassScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);
        class_id = null;
        class_name = null;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras != null){
                class_id = extras.getString("class_id");
                class_name = extras.getString("name");
            }
        } else {
            class_id = savedInstanceState.getString("class_id");
            class_name = savedInstanceState.getString("name");
        }

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(class_name);

        fragmentClassDetail = new FragmentClassDetail();
        fragmentClassTime = new FragmentClassTime(class_id, class_name);
        fragmentClassTask = new FragmentClassTask(class_id, class_name);
        fragmentClassScore = new FragmentClassScore();

        viewPager = findViewById(R.id.c_viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.c_tabs);
        tabLayout.setupWithViewPager(viewPager);

        getClassData();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(fragmentClassDetail, "Detail");
        adapter.addFragment(fragmentClassTime, "Time");
        adapter.addFragment(fragmentClassTask, "Task");
        adapter.addFragment(fragmentClassScore, "Score");
        viewPager.setAdapter(adapter);
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);
    }

    public void getClassData(){
        //showProgress(true); //weird bug
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    //String message = o.getString("message");
                    if(status.equalsIgnoreCase("true")){
                        JSONObject data = new JSONObject(o.getString("data"));
                        fragmentClassDetail.onDataReceived(data.getString("detail"));

                        JSONArray time = new JSONArray(data.getString("time"));
                        fragmentClassTime.onDataReceived(time);
                        JSONArray score = new JSONArray(data.getString("score"));
                        JSONArray task = new JSONArray(data.getString("task"));
                        fragmentClassTask.onDataReceived(task);
                        showProgress(false);
                    }
                    else if(status.equalsIgnoreCase("false")){ //jika data 0 masuk sini juga
                        //String message = o.getString("message");
                        //Toast.makeText(AddClassActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(ClassActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(ClassActivity.this, o.toString(), Toast.LENGTH_LONG).show();
                    }
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(ClassActivity.this, "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "Classes/class";

        String ApiKey = getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);
        params.put("class_id", class_id);

        VolleyHelper.getInstance(this).get(url, ofl, params);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        final View appView = ClassActivity.this.findViewById(R.id.c_viewpager);
        final View progressView = ClassActivity.this.findViewById(R.id.c_progressBar);
        InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ClassActivity.this.getWindow().getDecorView().getWindowToken(), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            appView.setVisibility(show ? View.GONE : View.VISIBLE);
            appView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    appView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            appView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    //region Fragment Data Listener
    private OnAboutDataReceivedListener mAboutDataListener;

    public interface OnAboutDataReceivedListener {
        void onDataReceived(Object data);
    }

    public void setAboutDataListener(OnAboutDataReceivedListener listener) {
        this.mAboutDataListener = listener;
    }
    //endregion
}

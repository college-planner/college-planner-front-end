package com.collegeplanner.collegeplanner.Fragment.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.collegeplanner.collegeplanner.AddClassActivity;
import com.collegeplanner.collegeplanner.ClassActivity;
import com.collegeplanner.collegeplanner.Fragment.Main.AbstractFragmentMain;
import com.collegeplanner.collegeplanner.Fragment.Main.AdapterRecyclerClasses;
import com.collegeplanner.collegeplanner.Helper.OnFinishListener;
import com.collegeplanner.collegeplanner.Helper.VolleyHelper;
import com.collegeplanner.collegeplanner.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FragmentClasses extends AbstractFragmentMain {
    private RecyclerView mRecyclerView;
    private AdapterRecyclerClasses mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<Map<String, String>> classes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_classes, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        classes = new ArrayList<>();

        FloatingActionButton fab = view.findViewById(R.id.fab_add_classes);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddClassActivity.class);
                startActivity(i);
            }
        });

        mRecyclerView = view.findViewById(R.id.fc_recyclerView);


        mRecyclerView.setHasFixedSize(true);


        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new AdapterRecyclerClasses(new AdapterRecyclerClasses.RecyclerViewClickListener(){
            @Override
            public void onClick(View view, int position) {
                String class_id = classes.get(position).get("id");
                String name = classes.get(position).get("name");
                Intent i = new Intent(FragmentClasses.this.getActivity(), ClassActivity.class);
                i.putExtra("class_id", class_id);
                i.putExtra("name", name);
                startActivity(i);
            }
        });

        mRecyclerView.setAdapter(mAdapter);

        getAllClass();
    }

    private void getAllClass(){
        showProgress(true);
        OnFinishListener ofl = new OnFinishListener(){
            @Override
            public void onFinish( JSONObject o ) {
                try {
                    String status = o.getString("status");
                    if(status.equalsIgnoreCase("true")){
                        ArrayList<Map<String, String>> classesName = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(o.getString("data"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String semester_id = jsonObject.getString("semester_id");
                            String name = jsonObject.getString("name");
                            String detail = jsonObject.getString("detail");
                            String color = jsonObject.getString("color");
                            String gpa = jsonObject.getString("gpa");
                            String credit = jsonObject.getString("credit");
                            Map<String, String> dataList = new HashMap<>();
                            dataList.put("id", id);
                            dataList.put("semester_id", semester_id);
                            dataList.put("name", name);
                            dataList.put("detail", detail);
                            dataList.put("color", color);
                            dataList.put("gpa", gpa);
                            dataList.put("credit", credit);
                            classes.add(dataList);

                            Map<String, String> classesNameTemp = new HashMap<String, String>();
                            classesNameTemp.put("data", name);
                            classesNameTemp.put("color", color);
                            classesName.add(classesNameTemp);
                        }
                        mAdapter.updateData(classesName);
                    }
                    else if(status.equalsIgnoreCase("false")){

                    }
                    else{
                        Log.e(getClass().getName(), "onFinish: " + o.toString());
                        Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getContext(), o.toString(), Toast.LENGTH_LONG).show();
                    }
                    showProgress(false);
                }
                catch(JSONException e){
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Oops, something wrong", Toast.LENGTH_SHORT).show();
                }
            }
        };
        String url = getString(R.string.webservice) + "Classes/allclass_user";

        String ApiKey = getContext().getSharedPreferences(getString(R.string.preference_api_key), Context.MODE_PRIVATE).getString(getString(R.string.preference_api_key), "NULL");

        Map<String, String> params = new HashMap<>();
        params.put("key", ApiKey);

        VolleyHelper.getInstance(getContext()).get(url, ofl, params);
    }
}

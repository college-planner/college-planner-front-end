package com.collegeplanner.collegeplanner.Fragment.Main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import com.collegeplanner.collegeplanner.AddTaskActivity;
import com.collegeplanner.collegeplanner.R;
import com.github.clans.fab.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class FragmentCalendar extends AbstractFragmentMain {

    CalendarView calendarView;
    FragmentActivity listener;
    TextView textDate;
    FloatingActionButton fabAddTask;

    // This event fires 1st, before creation of fragment or any views
    // The onAttach method is called when the Fragment instance is associated with an Activity.
    // This does not mean the Activity is fully initialized.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            this.listener = (FragmentActivity) context;

        }
    }

    // This method is called when the fragment is no longer connected to the Activity
    // Any references saved in onAttach should be nulled out here to prevent memory leaks.
    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
//        return inflater.inflate(R.layout.fragment_calendar, parent, false);
        View v = inflater.inflate(R.layout.fragment_calendar, parent, false);
        fabAddTask = (FloatingActionButton) v.findViewById(R.id.fc_fab_add_task);
        return v;
    }


    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        textDate = view.findViewById(R.id.fc_textDate);
        fabAddTask = view.findViewById(R.id.fc_fab_add_task);
        calendarView = view.findViewById(R.id.fc_calendarView);

        fabAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddTaskActivity.class);
                startActivity(i);
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int date) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DATE, date);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.YEAR, year);
                updateTextDate(calendar.getTime());
            }
        });

        updateTextDate(Calendar.getInstance().getTime());
    }

    public void updateTextDate(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.date_format_full));
        String formattedDate = simpleDateFormat.format(date);

        textDate.setText(formattedDate);
    }


}

package com.collegeplanner.collegeplanner.DB;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "year", foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user_id"))

public class Year {
    @PrimaryKey
    public int id;

    public int user_id;

    public Double gpa;

    public String name;
}